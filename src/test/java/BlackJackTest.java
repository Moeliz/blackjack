import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

//import main.java.BlackJack;
//import main.java.Card;
//import main.java.Deck;
//import main.java.Rank;
//import main.java.Suit;

@TestInstance(Lifecycle.PER_CLASS)
public class BlackJackTest {

	BlackJack blackjack = BlackJack.getInstance();
	Deck deck = new Deck();

	@BeforeEach
	void resetHand() {
		blackjack.playerHand.clear();
	}

	@Test
	void testScore() {

		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.TWO, Suit.S));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 13, "Ace of hearts + 2 of spades");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.NINE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.TEN, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 19, "9 of hearts + 10 of hearts");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.TEN, Suit.H));
		blackjack.playerHand.add(new Card(Rank.JACK, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 20, "10 of hearts + Jack of hearts");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.QUEEN, Suit.H));
		blackjack.playerHand.add(new Card(Rank.KING, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 20, "Queen of hearts + King of hearts");

	}

	@Test
	void testAces() {
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.S));
		blackjack.playerHand.add(new Card(Rank.KING, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 12,
				"Ace of hearts + ace of spades + King of hearts");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.S));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.D));
		blackjack.playerHand.add(new Card(Rank.KING, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 13,
				"Ace of hearts + ace of spades + ace of diamonds + King of hearts");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.S));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.D));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.C));
		blackjack.playerHand.add(new Card(Rank.KING, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 14,
				"Ace of hearts + ace of spades + ace of diamonds + ace of clubs + King of hearts");

		blackjack.playerHand.clear();
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.H));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.S));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.D));
		blackjack.playerHand.add(new Card(Rank.ACE, Suit.C));
		blackjack.playerHand.add(new Card(Rank.SEVEN, Suit.H));
		assertEquals(true, blackjack.score(blackjack.playerHand) == 21,
				"Ace of hearts + ace of spades + ace of diamonds + ace of clubs + 7 of hearts");

	}

	@Test
	void testNoDuplicateCardsWithoutShuffle() {
		List<Card> deck = blackjack.deck.getDeck();
		while (deck.size() > 0) {
			Card card = deck.get(0);
			deck.remove(0);
			for (int i = 0; i < deck.size(); i++) {
				assertFalse(card.equals(deck.get(i)));
			}
		}

	}

	@Test
	void testNoDuplicateCardsWithShuffle() {
		Deck deck = new Deck();
		deck.shuffle();
		while (deck.getDeck().size() > 0) {
			Card card = deck.draw();
			for (int i = 0; i < deck.getDeck().size(); i++) {
				assertNotEquals(true, card.equals(deck.getDeck().get(i)));
			}
		}

	}

	@Test
	void test53Cards() {
		Deck deck = new Deck();
		while (!deck.getDeck().isEmpty()) {
			deck.draw();
		}
		assertNull(deck.draw());
	}

	@ParameterizedTest
	@ValueSource(strings = { "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "JACK", "QUEEN",
			"KING", "ACE" })
	void testRankSuits(Rank argument) {
		assertEquals(true, new Card(argument, Suit.H).equals(new Card(argument, Suit.H)));

	}


	@Test
	void testAllRanksAndSuits() {
		for (Suit suit : Suit.values()) {
			for (Rank name : Rank.values()) {

				assertEquals(true, (new Card(name, suit).equals(new Card(name, suit))));
			}
		}
	}

	@Test
	void testEquals() {

		assertEquals(true, new Card(Rank.TWO, Suit.H).equals(new Card(Rank.TWO, Suit.H)));

	}

	@Test
	void testNotEquals() {
		assertNotEquals(true, new Card(Rank.TWO, Suit.H).equals(new Card(Rank.TWO, Suit.S)));

	}

	@Test
	void testDeck52CardsBeforAndAfterShuffle() {
		Deck deck = new Deck();
		assertTrue(deck.getDeck().size() == 52);
		deck.shuffle();
		assertTrue(deck.getDeck().size() == 52);

	}

	@RepeatedTest(100)
	void testDrawAndDeckSizeBeforeAndAfterShuffle() {
		deck.draw();
		int decksizebeforshuffle = deck.getDeck().size();
		deck.shuffle();
		assertTrue(decksizebeforshuffle == deck.getDeck().size());
	}

}
