public class Card {
	private Rank name;
	private Suit suit;

	public Card() {
	}

	public Card(Rank name, Suit suit) {
		this.name = name;
		this.suit = suit;

	}

	public Rank setValue(Rank name) {
		this.name = name;
		return name;
	}

	public int getValue() {

		return name.getValue();

	}

	public Suit getRankSuit() {
		return this.suit;
	}

	public String getSuit() {

		return suit.getSuit();
	}

//	public Rank getRankName() {
//		return this.name;
//
//	}

	public String getName() {
		return name.getName();
	}

	@Override
	public boolean equals(Object object) {
		if ((Card) object == null) {
			return object == null;
		}
		return ((Card) object).name == name && ((Card) object).suit == suit;
	}

}
