import java.util.ArrayList;
import java.util.List;

public class BlackJack {

	private static BlackJack instance;
	public List<Card> playerHand = new ArrayList<>();
	public List<Card> dealerHand = new ArrayList<>();
	public Deck deck = new Deck();

	private BlackJack() {

	}

	public static BlackJack getInstance() {
		if (instance == null) {
			instance = new BlackJack();
		}
		return instance;
	}

	public void hit() {

		playerHand.add(deck.draw());
		printHand(playerHand);
		if (score(playerHand) > 21) {
			System.out.println(
					"Busted!\nDealer won!\nDealer got: " + score(dealerHand) + "\nPlayer got: " + score(playerHand));
		} else if (score(playerHand) == 21) {
			System.out.println("BlackJack, you won!");
		}
	}

	public void stand() {
		printHand(playerHand);

		while (score(dealerHand) < score(playerHand)) {
			dealerHand.add(deck.draw());
			printHand(dealerHand);
		}
		if (score(dealerHand) > 21) {
			System.out.println("You won!\nDealer Busted!\nDealer got: " + score(dealerHand) + "\nPlayer got: "
					+ score(playerHand));

		} else if (score(playerHand) > score(dealerHand)) {
			System.out.print("You won!\nDealer got: " + score(dealerHand) + "\nPlayer got: " + score(playerHand));

		} else if (score(dealerHand) == score(playerHand) || score(dealerHand) > score(playerHand)) {
			System.out.println("Dealer won!\nDealer got: " + score(dealerHand) + "\nPlayer got: " + score(playerHand));
			}

	}

	public void reset() {
		deck = new Deck();
		deck.shuffle();
		playerHand = new ArrayList<>();
		dealerHand = new ArrayList<>();
		playerHand.add(deck.draw());
		dealerHand.add(deck.draw());
		playerHand.add(deck.draw());
		printHand(playerHand);
		printHand(dealerHand);
		dealerHand.add(deck.draw());
		if (score(playerHand) == 21) {
			System.out.println("BlackJack, you won!");
		}

	}

	public int score(List<Card> hand) {
		int score = 0;
		int aces = 0;
		for (Card s : hand) {
			if (s.getName().equals(Rank.ACE.getName())) {

				aces++;
			} else {
				score += s.getValue();
			}
		}
		if (aces > 0) {
			int maxScore = score + 11 + aces - 1;

			if (maxScore <= 21) {
				return maxScore;
			}

			return score + aces;
		}

		return score;

	}

	public void printHand(List<Card> hand) {
		if (hand == playerHand) {
			System.out.println("Player hand: ");
		} else if (hand == dealerHand) {
			System.out.println("Dealer hand: ");
		}
		for (Card s : hand) {
			System.out.printf("%s %s\n", s.getName(), s.getSuit());

		}
	}

}
