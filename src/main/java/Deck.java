import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	private List<Card> cards = new ArrayList<Card>();

	public Deck() {
		for (Suit suit : Suit.values()) {
			for (Rank name : Rank.values()) {
				cards.add(new Card(name, suit));

			}
		}
	}

	public List<Card> getDeck() {
		return this.cards;

	}

	public Card draw() {
		if (this.cards.isEmpty()) {
			return null;
		}
		this.cards.get(0);
		return this.cards.remove(0);

	}

	public void shuffle() {
		Collections.shuffle(cards);

	}

}
