public enum Suit {
	H("\u2660"), S("\u2665"), D("\u2666"), C("\u2663");
	
	private String suit;
	
	private Suit(String icon) {
		this.suit = icon;
	}
	public String getSuit() {
		return suit;
	}
	


}
