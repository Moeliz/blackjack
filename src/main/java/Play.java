import java.util.Scanner;

public class Play {

	Scanner sc = new Scanner(System.in);

	boolean avsluta = false;

	public Play() {
		System.out.print("---------\n");
		System.out.printf("1. Hit\n" + "2. Stand\n" + "3. New Game\n" + "4. Cash Out\n");
		System.out.print("---------\n");
		BlackJack.getInstance().reset();
		do {
			int choice = sc.nextInt();
			switch (choice) {
			case 1:
				BlackJack.getInstance().hit();
				System.out.println("---------");
				break;
			case 2:
				BlackJack.getInstance().stand();
				break;
			case 3:
				System.out.println("New Game\n----------");
				BlackJack.getInstance().reset();
				break;
			case 4:
				avsluta = true;
				break;
			}
		} while (!avsluta);
		System.out.print("$$$$$$$$$");
		System.exit(0);
	}

}